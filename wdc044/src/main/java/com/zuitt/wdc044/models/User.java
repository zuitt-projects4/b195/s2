package com.zuitt.wdc044.models;

import javax.persistence.*;

@Entity
@Table(name="users")
public class User {
    //User models properties:
    //Id - Long / Primary Key / Auto - incrememnts
    @Id
    @GeneratedValue
    private  Long id;

    //username - String

    @Column
    private String password;
    
    //password - String
    @Column
    private String username;

    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    //Getter for id
    public Long getId() {
        return id;

    }

    //Getters and Setters for username and password


    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
